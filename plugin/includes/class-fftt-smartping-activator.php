<?php

/**
 * Fired during plugin activation
 *
 * @link       https://gitlab.com/gwaroquier
 * @since      1.0.0
 *
 * @package    Fftt_Smartping
 * @subpackage Fftt_Smartping/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Fftt_Smartping
 * @subpackage Fftt_Smartping/includes
 * @author     Geoffrey WAROQUIER <geoffrey.waroquier@gmail.com>
 */
class Fftt_Smartping_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
