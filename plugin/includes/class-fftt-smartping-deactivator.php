<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gitlab.com/gwaroquier
 * @since      1.0.0
 *
 * @package    Fftt_Smartping
 * @subpackage Fftt_Smartping/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Fftt_Smartping
 * @subpackage Fftt_Smartping/includes
 * @author     Geoffrey WAROQUIER <geoffrey.waroquier@gmail.com>
 */
class Fftt_Smartping_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
